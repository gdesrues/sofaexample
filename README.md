# Sofa Example

Launch the scenes with `runSofa scene.py` and `runSofa scene_flexible.py`.

See [graphFEM.txt](graphFEM.txt) and [graph_flexible.txt](graph_flexible.txt) to see the two scene structures.


## Scene FEM
Using the component `TetrahedronFEMForceField`

## Scene using the Flexible plugin
Three layers mechanics, see the [github doc](https://github.com/sofa-framework/sofa/blob/master/applications/plugins/Flexible/doc/Flexible.pdf)

## Comparison
![50 iterations simple ForceField](n=50.png)
