import Sofa
import os

class Scene(Sofa.PythonScriptController):
    def __init__(self, node):
        self.createGraph(node)
        return None


    def createGraph(self, rootNode):
        # rootNode
        self.rootNode = rootNode
        rootNode.dt = 0.01
        rootNode.gravity = "0 0 0"
        rootNode.createObject('VisualStyle', displayFlags='showVisual showForceFields showBehavior')#showWireframe
        rootNode.createObject('DefaultAnimationLoop')
        rootNode.createObject('DefaultVisualManagerLoop')
        rootNode.createObject('EulerImplicitSolver', rayleighStiffness='0.1', rayleighMass='0.1')
        rootNode.createObject('CGLinearSolver', iterations='100')


        # rootNode/Bar
        Bar = rootNode.createChild('Bar')
        self.Bar = Bar
        Bar.createObject('MeshVTKLoader', name='loader', filename='undeformed.vtk')
        Bar.createObject('Mesh', name="mesh", src="@loader")
        Bar.createObject('MechanicalObject', name='dof', template="Vec3d")
        Bar.createObject('TetrahedronFEMForceField', poissonRatio=0.35, youngModulus=15e3)
        Bar.createObject('UniformMass', totalMass='10')

        Bar.createObject("VTKExporter", filename="deformed.vtk", XMLformat="false",
                        edges=0, triangles=0, tetras=1,
                        exportAtEnd="1", position="@dof.position", overwrite="1")




        # rootNode/Bar/DirichletBoundary
        DirichletBoundary = Bar.createChild('DirichletBoundary')
        self.DirichletBoundary = DirichletBoundary
        DirichletBoundary.createObject('BoxROI', box='0 0 0 0 1 1', position='@../mesh.position',
                                    template='Vec3d', drawSize="10", drawPoints="1",
                                    drawTriangles='0', triangles="@../loader.triangles",
                                    drawEdges="0", edges="@../loader.edges")
        DirichletBoundary.createObject('RestShapeSpringsForceField', points='@[-1].indices', template='Vec3d', stiffness='1E10')



        # rootNode/Bar/PressureBoundary
        PressureBoundary = Bar.createChild('PressureBoundary')
        self.PressureBoundary = PressureBoundary
        PressureBoundary.createObject('TriangleSetTopologyContainer', triangles="@../loader.triangles")
        PressureBoundary.createObject('TriangleSetTopologyModifier')
        PressureBoundary.createObject('TriangleSetTopologyAlgorithms')
        PressureBoundary.createObject('TriangleSetGeometryAlgorithms')
        PressureBoundary.createObject('BoxROI', box='0 0 0 10 1 0', position='@../mesh.position',
                                    template='Vec3d', drawSize="5", drawPoints="0",
                                    drawTriangles='1', triangles="@../loader.triangles",
                                    drawEdges="0", edges="@../loader.edges")
        PressureBoundary.createObject('TrianglePressureForceField', name="TPFF",
                                    triangleList="@[-1].triangleIndices",
                                    pressure="0 0 3",
                                    showForces="1",
                                    useConstantForce="1",
                                    normal="0 -1 0")



        # rootNode/Bar/visual
        visual = Bar.createChild('visual')
        self.visual = visual
        visual.createObject('MeshSTLLoader', name='stlloader', filename='undeformed.stl')
        visual.createObject('OglModel', name='Visual', template='ExtVec3f', src='@stlloader', color='gray')
        visual.createObject('BarycentricMapping', template='Vec3d,ExtVec3f')

        return 0



def createScene(rootNode):
    myScene = Scene(rootNode)
    return 0








#
